//===========================================================================
//===========================================================================
//===========================================================================
//==  UP2Best. Author: Costin-Anton BOIANGIU
//===========================================================================
//===========================================================================
//===========================================================================

#include "stdafx.h"
#include "Direct_Access_Image.h"
#include "Resample.h"
#include "Filter.h"
#include "Test.h"
#include "FileUtils.hpp"

#undef  USE_FORMAT

// do not use format by default
#define USE_FORMAT 1

#undef SAVE_IMAGES

// do not save images by default
#define SAVE_IMAGES false

int _tmain( int argc, _TCHAR *argv[] ) {
    if ( argc != 4 ) {
        _tprintf( _T( "Invalid program usage, correct syntax is: %s " )
                  _T( "<Input Folder> <Output Folder> <Output File> <CR>!\n" ),
                  argv[ 0 ] );
        getchar();
        return -1;
    }

    std::wstring inFolder = ToWstring( argv[ 1 ] );
    std::wstring outFolder = ToWstring( argv[ 2 ] );
    std::wstring outFile  = ToWstring( argv[ 3 ] );

    std::vector<std::wstring> files = GetFileList( argv[ 1 ] );
    std::wstring resultFileName = outFolder + PATH_SEPARATOR +
        outFile;
    
#if USE_FORMAT != 1
    resultFileName += std::wstring( L".csv" );
#else
    resultFileName += std::wstring( L".txt" );
#endif

    SetPathSize( resultFileName );

    Result results;

    size_t maxFileNameLength = 0;
    for ( size_t fileIndex = 0; fileIndex < files.size(); ++fileIndex ) {
        auto &fileName = files[ fileIndex ];
        if ( maxFileNameLength < fileName.length() ) {
            maxFileNameLength = fileName.length();
        }
    }

    for ( size_t fileIndex = 0; fileIndex < files.size(); ++fileIndex ) {
        auto &fileName = files[ fileIndex ];
        auto filePath = inFolder + PATH_SEPARATOR + fileName;
        SetPathSize( filePath );

        KImage *pImage = new KImage( filePath.c_str() );
        if ( !pImage->IsValid() || pImage->GetBPP() != 8 ) {
            delete pImage;
            continue;
        }

        auto width = pImage->GetWidth();
        auto height = pImage->GetHeight();
        TestInput baseInput( 0, 0, pImage, width, height, fileName,
                             outFolder );

        RunTestCase( results, baseInput, SAVE_IMAGES );

        delete pImage;
    }

    results.Save( resultFileName, maxFileNameLength, USE_FORMAT );

    long double dblMSEAvg = results.GetAvgMSE();
    long double dblPSNRAvg = PSNR( dblMSEAvg );

    _tprintf( _T( "After %d stats: AVG_MSE = %.3lf, AVG_PSNR = %.3lfdB\n" ),
              results.GetNumTests(), dblMSEAvg, dblPSNRAvg );

    system( "pause" );

    getchar();
    return 0;
}