#ifndef __FILEUTILS_HPP__
#define __FILEUTILS_HPP__

#include <tchar.h>
#include <vector>

#define PATH_SEPARATOR std::wstring(L"\\")

std::vector<std::wstring> GetFileList(_TCHAR *folderPath);
void SetPathSize(std::wstring &path);

std::wstring ToWstring(TCHAR *_path);
#endif
