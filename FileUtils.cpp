#include "stdafx.h"

#include "FileUtils.hpp"

#include <vector>


std::vector<std::wstring> GetFileList(_TCHAR *folderPath) {
    std::vector<std::wstring> files;

    TCHAR szFileMask[ _MAX_PATH ];

    //_tcscpy_s(szInputPath, folderPath);
    _stprintf_s(szFileMask, _MAX_PATH, _T( "%s\\*.TIF" ), folderPath);
    _tfinddata_t FindData;

    intptr_t handleI, handleJ;

    for (handleI = handleJ = _tfindfirst(szFileMask, &FindData); 
          handleI != -1; handleI = _tfindnext(handleJ, &FindData)) {
        if ((FindData.attrib & _A_SUBDIR) != 0)
            continue;

        files.push_back(std::wstring(FindData.name));
    }

    _findclose(handleJ);

    return files;
}


void SetPathSize(std::wstring &path) {
    // limit file path length to _MAX_PATH
    if (path.size() > _MAX_PATH)
        path.resize(_MAX_PATH);
}

std::wstring ToWstring(TCHAR *_path) {
    std::wstring path(_path);
    SetPathSize(path);
    return path;
}
