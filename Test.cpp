#include "stdafx.h"

#include "Test.h"

#include "Resample.h"
#include "Filter.h"

#include <sstream>
#include <iomanip>
#include <fstream>
#include <iostream>
#include <algorithm>

void TestImage( TestData &testData ) {
    auto &in = testData.in;   // input data
    auto &out = testData.out; // output data

    auto srcW  = in.width;  // source image width
    auto srcH  = in.height; // source image height
    auto scale = in.scale;  // down/upsample scale

    auto dstW = int( srcW / scale + 0.5 ); // destination image width
    auto dstH = int( srcH / scale + 0.5 ); //destination image height

    out.downSampled = new KImage( dstW, dstH, 8 );
    out.upSampled   = new KImage( in.width, in.height, 8 );

    // downsample image
    Resample( in.source, out.downSampled, in.filterId );

    // upsample previous downsampeld image
    Resample( out.downSampled, out.upSampled, in.filterId );

    // compute mse
    out.mse = MSE( in.source, out.upSampled );
}

void SaveImages( std::wstring sourceFile, std::wstring outputFolder,
                      TestData &testData ) {
    TCHAR fileName[ _MAX_PATH ];
    int filterType = testData.in.filterId;
    auto filterName = Filters[ filterType ].filterName;
    double scale = testData.in.scale;

    _stprintf_s( fileName, _MAX_PATH,
                 _T( "%s\\%s_%s_%.2lf_DOWNSAMPLED.TIF" ),
                 outputFolder.c_str(), sourceFile.c_str(), filterName, scale );
    testData.out.downSampled->SaveAs( fileName );

    _stprintf_s( fileName, _MAX_PATH,
                 _T( "%s\\%s_%s_%.2lf_%.3lf.TIF" ),
                 outputFolder.c_str(), sourceFile.c_str(),
                 filterName, scale, testData.out.mse );
    testData.out.upSampled->SaveAs( fileName );
}

static TestData GetTestData( TestInput &baseInput, double scale,
                             int filterId ) {
    TestData testData;

    testData.in = baseInput;
    testData.in.scale = scale;
    testData.in.filterId = filterId;

    return testData;
}

void RunTestCase( Result &results, TestInput baseInput, bool saveImages ) {
    auto numTestCases = testSuite.numCases;

    auto &fileName  = baseInput.fileName;
    auto &outFolder = baseInput.outFolder;

    for ( int caseI = 0; caseI < numTestCases; ++caseI ) {
        auto tc = testSuite.cases[ caseI ];
        auto scale = tc.scale;

        for ( int filterId = 0; filterId < tc.numFilters; filterId++ ) {
            auto testData = GetTestData( baseInput, scale, filterId );
            TestImage( testData );
            if ( saveImages ) {
                SaveImages( fileName, outFolder, testData );
            }

            delete testData.out.downSampled;
            delete testData.out.upSampled;

            auto filterName = Filters[ filterId ].filterName;
            auto mse = testData.out.mse;

            results.AddResult( filterId, fileName, filterName, mse, scale );
        }
    }
}

TestResult::TestResult( int filterId, 
                        const std::wstring &fileName,
                        const std::wstring &filterName, long double mse,
                        double scale )
                        : filterId(filterId),fileName( fileName ), 
                        filterName( filterName ), mse( mse ), scale( scale )
{}

void Result::AddResult( int filterId, const std::wstring &fileName,
                        const std::wstring &filterName, long double mse,
                        double scale ) {
    results.push_back( TestResult( filterId, fileName, filterName, mse,
        scale ) );
    totalMSE += mse;
    
    auto numScaleDigits = log10( scale );
    if ( maxScaleLength < numScaleDigits ) {
        maxScaleLength = size_t( numScaleDigits );
    }

    auto numMSEDigits  = log10( mse );
    if ( maxMSELength < numMSEDigits ) {
        maxMSELength = size_t( numMSEDigits );
    }
}

long double Result::GetAvgMSE() const {
    return totalMSE / results.size();
}


size_t Result::GetNumTests() {
    return results.size();
}

static void SaveResult(size_t index, std::vector<TestResult> &results,
                         const TCHAR *delimiter, std::wofstream &outFile) {
        auto &result = results[ index ];

        outFile << result.fileName << delimiter;
        outFile << result.scale << delimiter;
        outFile << result.mse << delimiter;
        outFile << result.filterName << delimiter;
        outFile << std::endl;
}

static void SaveResultFormatted(size_t index, std::vector<TestResult> &results,
                         size_t scaleLength, size_t mseLength,
                         size_t maxFileNameLength,
                         const TCHAR *delimiter,
                         std::wofstream &outFile) {
        auto &result = results[ index ];

        outFile << std::setw( maxFileNameLength ) << std::setfill( L' ' );
        outFile << result.fileName << delimiter;

        outFile << std::setw( scaleLength ) << std::setfill( L' ' );
        outFile << std::fixed << std::setprecision( 2 );
        outFile << result.scale << delimiter;
        
        outFile << std::setprecision( 3 ) << std::setw( mseLength );
        outFile << std::setfill( L' ' );
        outFile << result.mse << delimiter;

        outFile << result.filterName << delimiter;
        outFile << std::endl;

        if ( index < results.size() - 1 ) {
            auto &nextResult = results[ index + 1 ];
            if ( result.scale != nextResult.scale ) {
                outFile << std::endl;
            }
        }
}

void Result::Save( std::wstring &fileName, size_t maxFileNameLength, 
                   bool formatResult ) {
    Sort();

    std::wofstream outFile( fileName, std::ios::out );
    if ( !outFile ) {
        std::cerr << "Error opening result file" << std::endl;
        return;
    }

    const TCHAR *delimiter;
    if ( formatResult ) {
        delimiter = L", ";
    }
    else {
        delimiter = L",";
    }
    

    auto scaleLength = (maxScaleLength + 1) + 1 + 2;
    auto mseLength = (maxMSELength + 1) + 1 + 3;

    if ( formatResult ) {
        for ( size_t i( 0 ); i < results.size(); ++i ) {
            SaveResultFormatted( i, results, scaleLength, mseLength, maxFileNameLength,
                                 delimiter, outFile );
        }
    }
    else {
        for ( size_t i( 0 ); i < results.size(); ++i )
            SaveResult( i, results, delimiter, outFile );
    }

    outFile.close();
}


class ResultSorter {
public:
    bool operator() ( TestResult &a, TestResult &b ) {
        auto strOrder = a.fileName.compare( b.fileName );

        const double eps = 0.00001;

        if ( strOrder )  return strOrder < 0;
        if ( fabs(a.scale - b.scale) > eps ) return a.scale < b.scale;
        return a.mse < b.mse;
    }
};

void Result::Sort() {
    const ResultSorter sorter;
    std::sort( results.begin(), results.end(), sorter );
}