#ifndef __TEST_HPP__
#define __TEST_HPP__

#include "Direct_Access_Image.h"

#include <vector>

struct TestInput {
    TestInput() {}

    TestInput( int filterId, int scale, KImage *source,
               int width, int height, const std::wstring &fileName,
               const std::wstring &outFolder )
               : filterId( filterId ), scale( scale ), source( source ),
               width( width ), height( height ), fileName( fileName ),
               outFolder( outFolder ) {}

    TestInput( const TestInput &other ) {
        filterId  = other.filterId;
        scale     = other.scale;
        source    = other.source;
        width     = other.width;
        height    = other.height;
        fileName  = other.fileName;
        outFolder = other.outFolder;
    }

    TestInput & operator=( const TestInput &other ) {
        filterId  = other.filterId;
        scale     = other.scale;
        source    = other.source;
        width     = other.width;
        height    = other.height;
        fileName  = other.fileName;
        outFolder = other.outFolder;

        return *this;
    }

    int filterId = 0;
    int width    = 0;
    int height   = 0;

    double scale = 0.0;

    KImage *source = nullptr;

    std::wstring fileName;
    std::wstring outFolder;
};

struct TestOutput {
    KImage *downSampled = nullptr;
    KImage *upSampled = nullptr;
    long double mse;
};

struct TestData {
    TestInput in;
    TestOutput out;
};

struct TestCaseData {
    KImage *img = nullptr;
    std::wstring file;
    std::vector<TestData> data;
};

struct TestResult {
    TestResult( int filterId,
                const std::wstring &fileName,
                const std::wstring &filterName, long double mse,
                double scale );
    int filterId;
    std::wstring fileName;
    std::wstring filterName;
    long double mse;
    double scale;
};

class Result {
public:
    void AddResult( int filterId, const std::wstring &fileName,
                    const std::wstring &filterName, long double mse,
                    double scale );

    long double GetAvgMSE() const;

    size_t GetNumTests();

    void Save( std::wstring &fileName, size_t maxFileNameLength, bool format );

private:
    void Sort();

    std::vector<TestResult> results;
    long double totalMSE;
    size_t maxScaleLength = 0;
    size_t maxMSELength = 0;

};

void RunTestCase( Result &results, TestInput baseInput, bool saveImages );

void TestImage( TestData &testData );

void SaveImages( const std::wstring sourceFile,
                      const std::wstring output, TestData &testData );


#endif
